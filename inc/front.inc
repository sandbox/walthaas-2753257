<?php
/**
 * Generate the front page Utah state map of avalanche danger.
 */
    global $base_path;
    global $base_url;
    require_once(DRUPAL_ROOT . '/' . path_to_theme() . '/inc/advisory.inc');
    $regions = _get_regions();
    $data = _get_ratings($regions);
    $theme_path = $base_url . $base_path . path_to_theme();

    // For each geographic region where we produce an advisory, generate
    // a map overlay summarizing the danger in that region.
    $overlay = '';
    foreach ($data as $k => $v) {
      $detail = _get_advisory_data(node_load($v['nid']));
      $a = $detail['danger_rating'];
      $updated = format_date($v['changed'], 'custom', 'l - F j, Y');
      $overlay .=<<< EOT

              <img class="danger-overlay" src="$theme_path/img/drm/{$v['region_id']}-{$v['danger_rating']}.png" usemap="#regionmap" />
              <div id="tooltip_{$v['region_id']}" class="danger-rating-tooltip">
                <div class="darkbg">
                  <div class="danger-region"><span class="name">{$v['region_name']}</span><span class="updated">Issued $updated</span></div>
                  <div class="danger-rating-class"><span class="class-{$v['danger_rating']}">{$v['classification']}</span></div>
                  <div class="danger-rating-data"><img src="$theme_path/img/hover/danger-icon-{$v['danger_rating']}.png" /><div class="text">{$v['bottom_line']}</div></div>

                  <div class="danger-scale">
                    <img class="indicator-{$v['danger_rating']}" src="$theme_path/img/dr/danger-scale-indicator.png" />
                  </div>
                </div>
              </div>

EOT;
    }

?>
<!-- Generated from .../themes/uac_responsive/inc/front.inc -->
          <figure id="regionmap-figure">
            <figcaption>
              <h2>current danger ratings</h2>
              <p>
                Hover over an area to view the current danger rating for each region.
              </p><p>
                Click an area to view the full forecast.
              </p>
            </figcaption>
            <div id="regionmap-wrapper">
              <map id="regionmap" name="regionmap">
                <area id="area_1" shape="polygon" coords="207,1,239,1,245,46,233,65,187,67,178,32,184,30,194,55,205,53,204,15,207,8" href="/advisory/logan" />
                <area id="area_4" shape="polygon" coords="187,69,242,65,247,84,240,97,225,109,215,126,200,127,192,84" href="/advisory/ogden" />
                <area id="area_6" shape="polygon" coords="200,129,213,127,223,153,234,173,236,188,231,191,208,189,202,163,202,163" href="/advisory/salt-lake" />
                <area id="area_8" shape="polygon" coords="274,121,293,121,295,131,294,148,286,206,278,210,262,210,249,188,255,167,253,142,253,142" href="/advisory/uintas" />
                <area id="area_5" shape="polygon" coords="209,193,241,190,248,216,242,236,225,230" href="/advisory/provo" />
                <area id="area_7" shape="polygon" coords="248,252,265,254,273,271,275,310,256,347,246,351,223,350,218,344,221,340,229,315,241,293,238,266,242,259" href="/advisory/skyline" />
                <area id="area_2" shape="polygon" coords="452,401,451,436,444,441,436,441,427,432,423,416,426,407,433,403,440,401" href="/advisory/moab" />
                <area id="area_1475" shape="polygon" coords="174,334,184,365,220,375,243,396,213,422,196,414,185,436,169,438,146,467,124,526,83,527,97,504,129,479,138,455,134,438,138,407,149,404,149,404" href="/advisory/southwest" />
                <area id="area_1476" shape="polygon" coords="380,490,389,485,403,489,408,480,423,484,429,500,416,511,400,502,398,519,386,523,373,512,371,503,371,503" href="/advisory/abajo" />
              </map>
              <?php print($overlay); ?>
              <img id="utah-map" src="/<?php print path_to_theme() . '/img/drm/map.png'; ?>" usemap="#regionmap" />
            </div> <!-- /end #regionmap-wrapper -->
          </figure>
          <?php if ($content = render($page['content'])): ?>
          <div id="content" class="region">
            <?php print $content; ?>
          </div> <!-- end id="content" -->
          <?php endif; ?>
