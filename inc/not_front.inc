<!-- Generated from .../themes/uac_responsive/inc/not_front.inc -->
            <?php if ($content = render($page['content'])): ?>

              <!-- !Main Content Header -->
              <?php if ($title || $primary_local_tasks || $secondary_local_tasks || $action_links = render($action_links)): ?>
                <header<?php print $content_header_attributes; ?>>

                  <?php if ($title): ?>
                    <h1 id="page-title">
                      <?php print $title; ?>
                    </h1>
                  <?php endif; ?>

                  <?php if ($primary_local_tasks || $secondary_local_tasks || $action_links): ?>
                    <nav id="tasks">

                      <?php if ($primary_local_tasks): ?>
                        <ul class="tabs primary clearfix"><?php print render($primary_local_tasks); ?></ul>
                      <?php endif; ?>

                      <?php if ($secondary_local_tasks): ?>
                        <ul class="tabs secondary clearfix"><?php print render($secondary_local_tasks); ?></ul>
                      <?php endif; ?>

                      <?php if ($action_links = render($action_links)): ?>
                        <ul class="action-links clearfix"><?php print $action_links; ?></ul>
                      <?php endif; ?>

                    </nav>
                  <?php endif; ?>

                </header>
                <div id="content" class="region">
                  <?php print $content; ?>
                </div>
            <?php endif; ?>

              <!-- !Feed Icons -->
              <?php print $feed_icons; ?>

              <?php print render($title_suffix); // Prints page level contextual links ?>
            <?php endif; ?>
