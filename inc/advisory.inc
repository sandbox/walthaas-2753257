<?php

/**
 * @file
 * Functions for avalanche advisory data.
 */

/**
 * Extract data from advisory node into an array.
 *
 * @param Object $node  Node containing advisory information.
 * @return mixed[]  Array containing advisory information.
 */
function _get_advisory_data($node) {
  $a = array();
  if (!is_object($node)) {
    return $a;
  }

  $labels = array('None', '1. Low', '2. Moderate', '3. Considerable', '4. High', '5. Extreme');

  // These should really exist within a Drupal settings page, but this will do for now.
  $elevations = array(
    1 => array('5,000-7,000 ft.', '7,000-8,500 ft.', 'Above 8,500 ft.'), // Logan
    2 => array('Below treeline', 'Near treeline', 'Above treeline'), // Moab
    4 => array('Below 7,000 ft.', '7,000-8,500 ft.', 'Above 8,500 ft.'), // Ogden
    5 => array('Below 8,000 ft.', '8,000-9,500 ft.', 'Above 9,500 ft.'), // Provo
    6 => array('Below 8,000 ft.', '8,000-9,500 ft.', 'Above 9,500 ft.'), // Salt Lake
    7 => array('Below treeline', 'Near treeline', 'Above treeline'), // Skyline
    8 => array('Below treeline', 'Near treeline', 'Above treeline'), // Uintas
    1475 => array('Below 8,000 ft.', '8,000-9,500 ft.', 'Above 9,500 ft.'), // Southwest
    1476 => array('Below treeline', 'Near treeline', 'Above treeline'), // Abajo
  );

  $region_id = $node->field_region_forecaster['und'][0]['tid'];
  $a['region_id'] = $region_id;
  $user_fields = user_load($node->uid);
  $forecaster = '';
  if (!empty($user_fields->field_user_first_name['und'][0]['safe_value'])) {
    $forecaster = $user_fields->field_user_first_name['und'][0]['safe_value'];
  }
  if (!empty($user_fields->field_user_last_name['und'][0]['safe_value'])) {
    $forecaster .= ' ' . $user_fields->field_user_last_name['und'][0]['safe_value'];
  }
  if (empty($forecaster)) {
    $forecaster = $user_fields->name;
  }
  $a['forecaster'] = $forecaster;
  $a['title'] = $node->title;
  $a['date'] = format_date($node->created, 'custom', 'l - F j, Y - g:ia');
  $a['elevation_labels'] = array(
      'upper' => $elevations[$region_id][2],
      'mid' => $elevations[$region_id][1],
      'lower' => $elevations[$region_id][0],
  );

  // Danger ratings
  if (!empty($node->field_danger_rating_3)) {
    $a['danger_rating']['upper'] = $node->field_danger_rating_3['und'][0]['value'];
    $a['danger_rating']['upper_label'] = $labels[$a['danger_rating']['upper']];
    $a['danger_rating']['upper_elevation'] = $a['elevation_labels']['upper'];
  }

  if (!empty($node->field_danger_rating_2)) {
    $a['danger_rating']['mid'] = $node->field_danger_rating_2['und'][0]['value'];
    $a['danger_rating']['mid_label'] = $labels[$a['danger_rating']['mid']];
    $a['danger_rating']['mid_elevation'] = $a['elevation_labels']['mid'];
  }

  if (!empty($node->field_danger_rating_1)) {
    $a['danger_rating']['lower'] = $node->field_danger_rating_1['und'][0]['value'];
    $a['danger_rating']['lower_label'] = $labels[$a['danger_rating']['lower']];
    $a['danger_rating']['lower_elevation'] = $a['elevation_labels']['lower'];
  }

  if (!empty($node->field_danger_rating_current)) {
    $a['danger_rating']['current'] = $node->field_danger_rating_current['und'][0]['value'];
  }

  $ppattern = "#&nbsp;#i";

  // All other fields, optional
  if (!empty($node->field_bottom_line)) {
    $a['bottom_line'] = $node->field_bottom_line['und'][0]['value'];
    $a['bottom_line'] = preg_replace($ppattern, ' ', $a['bottom_line']);
  }

  if (!empty($node->field_special_announcement)) {
    $a['special_announcement'] = $node->field_special_announcement['und'][0]['value'];
    $a['special_announcement'] = preg_replace($ppattern, ' ', $a['special_announcement']);
  }

  if (!empty($node->field_general_announcements)) {
    $a['general_announcements'] = $node->field_general_announcements['und'][0]['value'];
  }

  if (!empty($node->field_current_conditions)) {
    $a['current_conditions'] = $node->field_current_conditions['und'][0]['value'];
    $a['current_conditions'] = preg_replace($ppattern, ' ', $a['current_conditions']);
  }

  if (!empty($node->field_recent_activity)) {
    $a['recent_activity'] = $node->field_recent_activity['und'][0]['value'];
    $a['recent_activity'] = preg_replace($ppattern, ' ', $a['recent_activity']);
  }

  if (!empty($node->field_mountain_weather)) {
    $a['mountain_weather'] = $node->field_mountain_weather['und'][0]['value'];
    $a['mountain_weather'] = preg_replace($ppattern, ' ', $a['mountain_weather']);
  }

  if (!empty($node->field_avalanche_warning)) {
    $a['avalanche_warning'] = $node->field_avalanche_warning['und'][0]['value'];
  }

  if (!empty($node->field_avalanche_watch)) {
    $a['avalanche_watch'] = $node->field_avalanche_watch['und'][0]['value'];
  }

  if (!empty($node->field_avalanche_sab)) {
    $a['avalanche_sab'] = $node->field_avalanche_sab['und'][0]['value'];
  }

  // Problem 1
  if (!empty($node->field_rose_1)) {
    $a['problem_1']['rose'] = $node->field_rose_1['und'][0];
  }

  if (!empty($node->field_duration_1)) {
    $a['problem_1']['duration'] = $node->field_duration_1['und'][0]['value'];
  }

  if (!empty($node->field_type_1)) {
    $a['problem_1']['type'] = $node->field_type_1['und'][0]['value'];
  }

  if (!empty($node->field_likelihood_1)) {
    $a['problem_1']['likelihood'] = $node->field_likelihood_1['und'][0]['value'];
  }

  if (!empty($node->field_size_1)) {
    $a['problem_1']['size'] = $node->field_size_1['und'][0]['value'];
  }

  /* if (!empty($node->field_distribution_1)) { $a['problem_1']['distribution'] = $node->field_distribution_1['und'][0]['value']; } */

  if (!empty($node->field_trend_1)) {
    $a['problem_1']['trend'] = $node->field_trend_1['und'][0]['value'];
  }

  if (!empty($node->field_description_1)) {
    $a['problem_1']['description'] = $node->field_description_1['und'][0]['value'];
    $a['problem_1']['description'] = preg_replace($ppattern, ' ', $a['problem_1']['description']);
  }

  // Problem 2
  if (!empty($node->field_rose_2)) {
    $a['problem_2']['rose'] = $node->field_rose_2['und'][0];
  }

  if (!empty($node->field_duration_2)) {
    $a['problem_2']['duration'] = $node->field_duration_2['und'][0]['value'];
  }

  if (!empty($node->field_type_2)) {
    $a['problem_2']['type'] = $node->field_type_2['und'][0]['value'];
  }

  if (!empty($node->field_likelihood_2)) {
    $a['problem_2']['likelihood'] = $node->field_likelihood_2['und'][0]['value'];
  }

  if (!empty($node->field_size_2)) {
    $a['problem_2']['size'] = $node->field_size_2['und'][0]['value'];
  }

  /* if (!empty($node->field_distribution_2)) { $a['problem_2']['distribution'] = $node->field_distribution_2['und'][0]['value']; } */

  if (!empty($node->field_trend_2)) {
    $a['problem_2']['trend'] = $node->field_trend_2['und'][0]['value'];
  }

  if (!empty($node->field_description_2)) {
    $a['problem_2']['description'] = $node->field_description_2['und'][0]['value'];
    $a['problem_2']['description'] = preg_replace($ppattern, ' ', $a['problem_2']['description']);
  }

  // Problem 3
  if (!empty($node->field_rose_3)) {
    $a['problem_3']['rose'] = $node->field_rose_3['und'][0];
  }

  if (!empty($node->field_duration_3)) {
    $a['problem_3']['duration'] = $node->field_duration_3['und'][0]['value'];
  }

  if (!empty($node->field_type_3)) {
    $a['problem_3']['type'] = $node->field_type_3['und'][0]['value'];
  }

  if (!empty($node->field_likelihood_3)) {
    $a['problem_3']['likelihood'] = $node->field_likelihood_3['und'][0]['value'];
  }

  if (!empty($node->field_size_3)) {
    $a['problem_3']['size'] = $node->field_size_3['und'][0]['value'];
  }

  /* if (!empty($node->field_distribution_3)) { $a['problem_3']['distribution'] = $node->field_distribution_3['und'][0]['value']; } */

  if (!empty($node->field_trend_3)) {
    $a['problem_3']['trend'] = $node->field_trend_3['und'][0]['value'];
  }

  if (!empty($node->field_description_3)) {
    $a['problem_3']['description'] = $node->field_description_3['und'][0]['value'];
    $a['problem_3']['description'] = preg_replace($ppattern, ' ', $a['problem_3']['description']);
  }

  return $a;
}

/**
 * Build a danger rose showing overall avalanche danger for region.
 *
 * @param type $rose
 * @param type $elevation_labels
 * @param type $bottom_line
 * @return string HTML
 */
// function _build_overall_rose($rose, $elevation_labels, $bottom_line) {
//   $data =<<<EOT
//   <!-- Generated from .../themes/uac_responsive/inc/advisory.inc:_build_overall_rose() -->
//   <div class="advisory-row advanced" id="problem-rose" style="padding-bottom:25px;">
//     <table class="row-sub-table">
//     <tr>
//       <td>
//       <a href="/danger-rose-tutorial"><img src="$theme_img/info.png" class="info"></a>
//       <img src="$rose">
//       </td>
//       <td><span class="uppercase">bottom line</span><br />$bottom_line
//       <br /><br /><br />
//       <a href="/avalanche-danger-scale"><img src="$theme_img/info.png" class="info" id="info-avalanche-danger-scale"></a>
//       <table id="avalanche-danger-scale">
//       <tr>
//         <th class="zcaption">danger scale:</th>
//         <th class="rating-1">Low</th>
//         <th class="rating-2">Moderate</th>
//         <th class="rating-3">Considerable</th>
//         <th class="rating-4">High</th>
//         <th class="rating-5">Extreme</th>
//       </table>
//       </td>
//     </table>
//   </div>
// EOT;

//   return $data;
// }

/**
 * Generate HTML describing a single avalanche problem.
 *
 * @param mixed[] $problem   Coded description of a single avalanche problem.
 * @param string $title  Title of this problem.
 * @param string[] $elevation_labels Labels for elevation bands of danger rose.
 * @return string HTML ready for output.
 */
function _build_problem_html($problem, $title = '', $elevation_labels) {
  $type = $problem['type'];
  if (!empty($problem['likelihood'])) {
    $likelihood = $problem['likelihood'];
  }
  else {
    $likelihood = 0;
  }
  if (!empty($problem['size'])) {
    $size = $problem['size'];
  }
  else {
    $size = 0;
  }
  if (!empty($problem['trend'])) {
    $trend = $problem['trend'];
  }
  else {
    $trend = 0;
  }
  $a = array('','Decreasing','Same','Increasing');
  $trend_text = $a[$trend];
  $description = $problem['description'];

  $info_type = '';
  switch ($type) {
      case '0': $info_type = 'normal-caution'; break;
      case '1': $info_type = 'storm-slab'; break;
      case '2': $info_type = 'persistent-slab'; break;
      case '3': $info_type = 'deep-slab'; break;
      case '4': $info_type = 'wet-slab'; break;
      case '5': $info_type = 'loose-dry-snow'; break;
      case '6': $info_type = 'loose-wet-snow'; break;
      case '7': $info_type = 'wind-slab'; break;
      case '8': $info_type = 'cornice-fall'; break;
      case '9': $info_type = 'glide'; break;
  }

  $theme_img = '/' . path_to_theme() . '/img';
$html = <<<EOT
<!-- Generated from .../themes/uac_responsive/inc/advisory.inc:_build_problem_html() -->
<div class="clearbg avy-prob-row">
  <h2 class="advisory-heading">$title</h2>
  <div class="avy-prob-table">
    <!-- Avalanche Problem Type -->
    <div class="avy-prob-type">
      <div class="avy-prob-heading-wrapper">
        <div class="avy-prob-heading">type</div>
      </div>
      <div class="avy-prob-type-cell">
        <div class="avy-prob-type-inner">
          <a href="/$info_type"><img src="$theme_img/info.png" class="info" /></a>
          <img src="$theme_img/api/$type.jpg" class="problem-type" />
        </div>
      </div>
    </div> <!-- class="avy-prob-type" -->
    <!-- Avalanche Problem Aspect/Elevation -->
    <div class="avy-prob-aspect-elev">
      <div class="avy-prob-heading-wrapper">
        <div class="avy-prob-heading">aspect/elevation</div>
      </div>
      <div class="avy-prob-aspect-elev-cell">
        <div class="avy-prob-aspect-elev-inner">
          <a href="/danger-rose-tutorial"><img src="$theme_img/info.png" class="danger-rose-info" /></a>
          <img class="problem-rose" src="{$problem['rose']['img_simplified_rose']}" />
        </div>
      </div>
    </div> <!-- class="avy-prob-aspect-elev" -->
    <!-- Avalanche Problem Characteristics -->
    <div class="avy-prob-char">
      <div class="avy-prob-heading-wrapper">
        <div class="avy-prob-heading">characteristics</div>
      </div>
          <!-- Avalanche Problem Characteristics: Likelihood -->
          <div class="avy-prob-char-likely-cell">
            <div class="avy-prob-char-likely-inner">
              <div class="avy-prob-char-header">LIKELIHOOD</div>
              <div class="avy-prob-char-likely-image-wrapper">
                <a href="/how-read-advisory#characteristics"><img src="$theme_img/info.png" class="info" /></a>
                <img src="$theme_img/likelihood/likelihood-$likelihood.gif" />
                <div class="avy-prob-text-likely">LIKELY</div>
                <div class="avy-prob-text-unlikely">UNLIKELY</div>
              </div>  <!-- class="avy-prob-char-likely-image-wrapper" -->
            </div>    <!-- class="avy-prob-char-likely-inner" -->
          </div>      <!-- class="avy-prob-char-likely-cell" -->
          <!-- Avalanche Problem Characteristics: Size -->
          <div class="avy-prob-char-size-cell">
            <div class="avy-prob-char-size-inner">
              <div class="avy-prob-char-header">SIZE</div>
              <div class="avy-prob-char-size-image-wrapper">
                <img src="$theme_img/size/size-$size.gif" />
                <div class="avy-prob-text-large">LARGE</div>
                <div class="avy-prob-text-small">SMALL</div>
              </div>  <!-- class="avy-prob-char-size-image-wrapper" -->
            </div>    <!-- class="avy-prob-char-size-inner" -->
          </div>      <!-- class="avy-prob-char-size-cell" -->
          <!-- Avalanche Problem Characteristics: Trend -->
          <div class="avy-prob-char-trend-cell">
            <div class="avy-prob-char-trend-inner">
              <div class="avy-prob-char-header">TREND</div>
              <div class="avy-prob-char-trend-image-wrapper">
                <img src="$theme_img/trend/trend-$trend.gif" />
                <div class="avy-prob-text-increasing">INCREASING&nbsp;DANGER</div>
                <div class="avy-prob-text-same">SAME</div>
                <div class="avy-prob-text-decreasing">DECREASING&nbsp;DANGER</div>
              </div>
              <span class="avy-prob-text-duration">over the next {$problem['duration']} hours</span>
            </div>    <!-- class="avy-prob-char-trend-inner" -->
          </div>     <!-- class="avy-prob-char-trend-cell" -->
    </div>   <!-- class="avy-prob-table" -->
  </div>     <!-- class="clearbg avy-prob-row" -->
  <div class="advisory-row">
    <h3 class="advisory-heading">description</h3>
    $description
  </div>
</div>
EOT;

  return $html;
}

/**
 * Get names of the geographic regions that we rate for avalanche danger.
 *
 * @return mixed[][] List of (tid, name) pairs.
 */
function _get_regions() {
  $sql = 'SELECT tid, name FROM {taxonomy_term_data} WHERE VID = 2 AND tid in (1,2,4,5,6,7,8,1475,1476)';
  // $sql = 'SELECT tid, name FROM {taxonomy_term_data} WHERE vid = 2 AND tid IN (SELECT tid FROM taxonomy_term_hierarchy WHERE parent = 0)';
  $result = db_query($sql);
  $regions = array();
  foreach ($result as $row) {
    $regions[$row->tid] = (array) $row;
  }

  return $regions;
}

/**
 * Get current avalanche danger ratings for specified regions.
 *
 * @param type $regions
 * @return type
 */
function _get_ratings($regions = array()) {
  $eids = implode(',', array_keys($regions));
  $sql =<<< EOT
SELECT max(n.nid) as node_id, r2.field_region_forecaster_tid AS region_id
FROM node n
LEFT JOIN field_data_field_region_forecaster r2 ON (r2.entity_id = n.nid)
WHERE n.type = 'advisory' AND n.status = 1 AND r2.field_region_forecaster_tid in (1,2,4,5,6,7,8,1475,1476)
GROUP BY r2.field_region_forecaster_tid
EOT;
  $data = array();
  $nids = array();
  $result = db_query($sql);
  foreach ($result as $row) { $nids[$row->node_id] = $row->node_id; }
  $nids = implode(',', array_keys($nids));

  $sql =<<< EOT
SELECT
  n.nid,
  n.changed,
  r1.field_danger_rating_current_value AS danger_rating,
  b.field_bottom_line_value AS bottom_line,
  r2.field_region_forecaster_tid AS region_id
FROM {node} n
LEFT JOIN {field_data_field_bottom_line} b ON (b.entity_id = n.nid)
LEFT JOIN {field_data_field_danger_rating_current} r1 ON (r1.entity_id = n.nid)
LEFT JOIN {field_data_field_region_forecaster} r2 ON (r2.entity_id = n.nid)
WHERE n.type = 'advisory'
AND n.status = 1
AND n.nid in ($nids)
AND r2.field_region_forecaster_tid in (1,2,4,5,6,7,8,1475,1476)
GROUP BY r2.field_region_forecaster_tid
ORDER BY
  r2.field_region_forecaster_tid,
  n.changed
EOT;

  $data = array();
  $result = db_query($sql);
  foreach ($result as $row) {
    if (empty($row->danger_rating)) {
      $row->danger_rating = 0;
    }
    $a = _get_default_data($row->danger_rating);
    $row->bottom_line = $a['bottom_line'];
    $row->classification = $a['classification'];
    $row->region_name = $regions[$row->region_id]['name'];
    $data[$row->region_id] = (array) $row;
  }

  return $data;
}

/**
 *  Get default values for advisory at specified danger level.
 *
 *  @param int danger level
 *  @return Array default values for that danger level
 */
function _get_default_data($i = 0) {
  $a = array();
  $i = (int) $i;
  switch ($i) {
    case 1:
      $a['bottom_line'] = 'Generally safe avalanche conditions. Watch for unstable snow on isolated terrain features.';
      $a['classification'] = 'LOW AVALANCHE DANGER';
      break;

    case 2:
      $a['bottom_line'] = 'Heightened avalanche conditions on specific terrain features. Evaluate snow and terrain carefully; identify features of concern.';
      $a['classification'] = 'MODERATE AVALANCHE DANGER';
      break;

    case 3:
      $a['bottom_line'] = 'Dangerous avalanche conditions. Careful snowpack evaluation, cautious route-finding and conservative decision-making essential.';
      $a['classification'] = 'CONSIDERABLE AVALANCHE DANGER';
      break;

    case 4:
      $a['bottom_line'] = 'Very dangerous avalanche conditions. Travel in avalanche terrain is not recommended.';
      $a['classification'] = 'HIGH AVALANCHE DANGER';
      break;

    case 5:
      $a['bottom_line'] = 'Avoid all avalanche terrain.';
      $a['classification'] = 'HIGH AVALANCHE DANGER';
      break;

    default:
      $a['bottom_line'] = 'Watch for signs of unstable snow such as recent avalanches, cracking in the snow, and audible collapsing. Avoid traveling on or under similar slopes.';
      $a['classification'] = 'NO RATING';
  }

  return $a;
}
