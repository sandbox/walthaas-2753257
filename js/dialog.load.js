// http://www.quirksmode.org/js/cookies.html
function readCookie(name) {
  var nameEQ = name + "=";
  var ca = document.cookie.split(';');
  for(var i=0;i < ca.length;i++) {
    var c = ca[i];
    while (c.charAt(0)==' ') c = c.substring(1,c.length);
    if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
  }
  return null;
}

function setCookie(val) {
  document.cookie = 'fuacdialog' + '=' + val + ';expires=Wed, 1 May 2020 00:00:00 GMT';
}

(function($) {
  $(document).ready(function() {
    $("#dialog-confirm").dialog({
      autoOpen: false,
      resizable: false,
      draggable: false,
      width: 600,
      maxWidth: 600,
      modal: true,
      position: ['middle', 50],
      buttons: {
        "Make a donation": function() {
          setCookie($("div#dialog-hash").text());
          window.location.href = "/donate";
        },
        "Close window": function() {
          setCookie($("div#dialog-hash").text());
          $(this).dialog("close");
        },
        "Remind me again": function() {
          $(this).dialog("close");
        }
      }
    });
    var c = readCookie('fuacdialog');
    if (c != $("div#dialog-hash").text()) {
      $("#dialog-confirm").dialog("open");
    }
  });
})(jQuery);
