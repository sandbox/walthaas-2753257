(function ($) {
  $(document).ready(function() {
    $('#regionmap area').tooltip({
        track: true,
        delay: 0,
        showURL: false,
        showBody: " - ",
        fade: 200,
        bodyHandler: function() { return $($(this).attr("id").replace('area_','#')).html(); }
    });
  });
})(jQuery);
