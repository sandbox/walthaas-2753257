/**
 * @file
 * Drupal JavaScript behaviors for the UAC Responsive theme.
 */

/**
 * When the page is loaded two important functions are performed:
 * 1. If the page has a clickable region map, ie. the map of Utah on the
 *    front page of  the site, the associated image map is updated to
 *    correct the clickable areas for the resizing of the map by the browser.
 *
 * 2. The page is checked for images with the 'style' attribute.  On each
 *    such image, the 'height' and 'width' styles are removed from the
 *    'style' attribute and assigned as image attributes of the respective
 *    name.  This is done so that the browser will scale height and width
 *    in the same proportion when adjusting the size of the image.  This is
 *    needed because CKEditor stores image styling in the 'style' attribute
 *    which browsers do not treat the same as the individual attributes.
 */

(function($) {
  "use strict";

  Drupal.behaviors.UACResponsive = {
    attach: function(context, settings) {

      // Use the presence of #regionmap on this page to indicate whether
      // there is a Utah map image with regions that must be scaled.
      // #regionmap is the map attribute for the Utah map.
      if ( $('#regionmap').length ) {
        $('#regionmap area', context).each(function (index, elem) {

          // Save the 'natural' coordinate values in jQuery data
          // attached to each area element.
          $(this).data('naturalCoords', $(this).attr('coords'));

          // Bind mouse event handlers to each area of the imagemap so
          // the relevant tooltip will appear on mouse hover.
          var tooltip = $(this).attr('id').replace('area', 'tooltip');
          var $tooltip = $('#' + tooltip);
          $(this).bind('mouseenter', null, function(evt) {
            var center = $('img#utah-map').width() / 2;
            var $target = $(evt.target);
            var left = center - ($tooltip.outerWidth(true) / 2);
            $tooltip.css('left', left + 'px');
            if ($target.data('loc') === 'a') {
              var top = $target.data('minY') - $tooltip.outerHeight(true) - 5;
            }
            else {
              var top = $target.data('maxY') + 5;
            }
            $tooltip.css('top', top + 'px');
            $tooltip.addClass("visible");
          });
          $(this).bind('mouseleave', null, function() {
            $tooltip.removeClass("visible");
          });
        });  // $('#regionmap area').each() {

        // We can't scale the image map until the image is fully loaded
        // so that we can find its dimensions, so trigger the scale
        // computation after the image is loaded.  Redundant resize events
        // are harmless.  Righteously this event should be triggered when
        // the Utah map loads, but that doesn't work in Google Chrome so
        // we must trigger on the window.
        $( window ).on('load', function(evt) {
          // Trigger a window resize event to run the scale computation.
          $(window).resize();
        });

        // When window is resized, rescale the image map so that the areas
        // where mouse hover is detected match the new size of the image.
        // NB: It's possible that resize will trigger before image is loaded.
        //   To deal with this, we trigger another resize when image loads.
        $(window).resize(function () {
          // Get the dimensions of the image independent of jQuery release.
          // Before jQuery 1.7 dimensions were in attributes.  From 1.7 on
          // dimensions are in properties.  Thanks a bunch jQuery team.
          //console.log('resize() called');
          var $img = $('img#utah-map');
          var height, naturalHeight;
          height = $img.height();
          if ('prop' in $img) {
            naturalHeight = $img.prop('naturalHeight');
          }
          else {
            naturalHeight = $img.attr('naturalHeight');
          }

          // The first window resize event may occur before the image is
          // loaded so that the image naturalHeight is still zero. If that
          // is the case in this call, return.  Another window resize will be
          // triggered when the image loads.
          if (naturalHeight === 0) {
            return;
          }

          // Scale the x, y coords for each area.
          var scale = height / naturalHeight;
          $('#regionmap area', context).each(function (index, elem) {
            var ncoords = $(this).data('naturalCoords').split(',');
            var scoords = String('');
            var first = true;
            var minY = 9999;
            var maxY = -1;
            for (var i = 0; i < ncoords.length; i++) {
              if (first) {
                first = false;
              }
              else {
                scoords = scoords.concat(',');
              }
              var x = ncoords[i++] * scale;
              var y = ncoords[i] * scale;
              scoords = scoords.concat(x, ',', y);
              minY = Math.min(y, minY);
              maxY = Math.max(y, maxY);
            }
            $(this).attr('coords', scoords);
            $(this).data('minY', minY);
            $(this).data('maxY', maxY);

            // Compare space above area to space below.
            // Record which is larger so the tooltip can be positioned in
            // the larger space.
            var spaceBelow = height - maxY;
            if (spaceBelow > minY) {
              $(this).data('loc', 'b');
            }
            else {
              $(this).data('loc', 'a');
            }
          });
        });  // $(window).resize()
      }      // if $('#regionmap')

      // Find every image with a style attribute.  Parse the height and
      // width attributes from the style and move them into regular
      // height and width so the image will be responsive.
      var style, htPat, wdPat, result;
      $("img[style]").each(function(index, elem) {
        style = $(elem).attr('style');
        // FIXME this matches line-height
        htPat = /height\s?:\s?(\d+)\w+\s?[;|$]?/i;
        wdPat = /width\s?:\s?(\d+)\w+\s?[;|$]?/i;
        result = style.match(htPat);
        if (result) {
          $(elem).attr('height', result[1]);
          style = style.replace(htPat, '');
        }
        result = style.match(wdPat);
        if (result) {
          $(elem).attr('width', result[1]);
          style = style.replace(wdPat, '');
        }
        style = style.trim();
        if (style) {
          $(elem).attr('style', style);
        }
        else {
          $(elem).removeAttr('style');
        }
      });
    }
  };
})(jQuery);

// Configure Emacs for Drupal JavaScript coding standards
// Local Variables:
// js2-basic-offset: 2
// indent-tabs-mode: nil
// fill-column: 78
// show-trailing-whitespace: t
// End:
