<?php
/**
 * @file
 * UAC Responsive implementation to display node 24460 containing
 * Ogden UT monthly weather charts.
 */

require_once(DRUPAL_ROOT . '/' . path_to_theme() . '/inc/node-common.inc');

?><!-- Begin: Generated from .../themes/uac_responsive/templates/node--24460.tpl.php -->

    <?php
      // Pull in stored dataset
      print "<strong>STATE WEATHER SYNOPSIS</strong>...";
      $s = file_get_contents(DRUPAL_ROOT . '/sites/default/files/data/wx/forecast/state.xml.ren');
      if ($s) { print "$s<br /><br />"; }
      print "<strong>Weather Forecast:  Near Ben Lomond (41.37, -111.94), Elevation: 8783'</strong><br />";
      $s = file_get_contents(DRUPAL_ROOT . '/sites/default/files/data/wx/forecast/ogden.json.ren');
      if ($s) { print "$s<br />"; }
      print render($content); ?>
  </div>

  <?php if ($links = render($content['links'])): ?>
    <nav id="link-wrapper"<?php print $links_attributes; ?>><?php print $links; ?></nav>
  <?php endif; ?>

  <?php print render($content['comments']); ?>

  <?php print render($title_suffix); ?>
</article>
<!-- End: Generated from .../themes/uac_responsive/templates/node--24460.tpl.php -->