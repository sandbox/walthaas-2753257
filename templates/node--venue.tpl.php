<?php
/**
 * @file
 * UAC Responsive implementation to display a Venue node.
 */

require_once(DRUPAL_ROOT . '/' . path_to_theme() . '/inc/node-common.inc');

?><!-- Begin: Generated from .../themes/uac_responsive/templates/node--venue.tpl.php -->
    <?php print render($content); ?>
</article>
<!-- End: Generated from .../themes/uac_responsive/templates/node--venue.tpl.php -->