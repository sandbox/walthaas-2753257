<?php

/**
 * @file
 * UAC Responsive template for the Evemts view formatted list views format.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains:
 *   - $field->content: The output of the field.
 *   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.
 *   - $field->class: The safe class id to use.
 *   - $field->handler: The Views field handler object controlling this field. Do not use
 *     var_export to dump this object, as it can't handle the recursion.
 *   - $field->inline: Whether or not the field should be inline.
 *   - $field->inline_html: either div or span based on the above flag.
 *   - $field->wrapper_prefix: A complete wrapper containing the inline_html to use.
 *   - $field->wrapper_suffix: The closing tag for the wrapper.
 *   - $field->separator: an optional separator that may appear before a field.
 *   - $field->label: The wrap label text to use.
 *   - $field->label_html: The full HTML of the label to use including
 *     configured element type.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates
 */
?>
<!-- Generated from .../themes/uac_responsive/templates/views-view-fields--events-page-6.tpl.php -->
<div class="event-col" id="event-col-1">
  <div id="month-day"><?php print($fields['field_date_time_1']->content); ?></div>
  <div id="time"><?php print($fields['field_date_time_1_1']->content); ?></div>
  <div id="venue-name"><?php print($fields['field_event_venue']->content); ?></div>
  <div id="venue-street"><?php print($fields['field_venue_street_address']->content); ?></div>
  <div id="venue-city-state-zip"><?php print($fields['field_venue_city_state_zip']->content); ?></div>
  <div id="venue-phone"><?php print($fields['field_venue_phone']->content); ?></div>
  <div id="venue-url"><?php print($fields['field_venue_url']->content); ?></div>
</div>
<div class="event-col" id="event-col-2">
  <div id="title"><?php print($fields['title']->content); ?></div>
  <div id="body"><?php print($fields['body']->content); ?></div>
</div>

