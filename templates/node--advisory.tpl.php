<?php
/**
 * @file
 * UAC Responsive implementation to display an avalanche advisory node.
 */

require_once(DRUPAL_ROOT . '/' . path_to_theme() . '/inc/node-common.inc');

?><!-- Begin: generated from .../themes/uac_responsive/templates/node--advisory.tpl.php -->

    <?php if (isset($advisory['bottom_line'])): ?>
      <?php require_once(path_to_theme() . '/inc/advisory.inc');
      $advisory = $variables['advisory'];
      $bottom_line = $advisory['bottom_line'];
      ?>
      <div id="advisory-avalanche-danger-row" class="advanced advisory-row">
        <div id="advisory-problem-rose" class="advisory-row advanced">
          <a href="/danger-rose-tutorial"><img src="<?php print '/' . path_to_theme() . '/img/info.png'; ?>" class="danger-rose-info"></a>
          <img src="<?php print $node->field_overall_rose['und'][0]['img_detailed_rose']; ?>" class="danger-rose">
        </div> <!-- id="advisory-problem-rose" -->
        <div id="advisory-bottom-line"><h3 class="advisory-heading">Bottom Line</h3><?php print $bottom_line; ?></div>
        <div id="advisory-danger-scale" class="clearfix">
          <a href="/avalanche-danger-scale"><img src="<?php print '/' . path_to_theme() . '/img/info.png'; ?>" class="info" id="info-avalanche-danger-scale"></a>
          <img src="<?php print '/' . path_to_theme() . '/img/danger-scale.png'; ?>">
        </div>
      </div> <!-- id="avalanche-danger-row" -->
    <?php endif; ?>

    <!--
    <div id="image-gallery-title-row" class="darkbg"><h3>gallery</h3></div>
    <div id="image-gallery-row" class="darkbg"></div>
    <div id="leaderboard"></div>
    -->

    <?php if (isset($advisory['avalanche_warning'])): ?>
      <!-- Avalanche Warning row -->
      <div id="warning-row" class="advisory-row">
        <img src="<?php print '/' . path_to_theme() . '/img/row/heading-awarning.jpg'; ?>" class="advisory-row-heading" />
        <h3 class="advisory-heading">avalanche warning</h3>
        <?php print $advisory['avalanche_warning']; ?>
      </div>
    <?php endif; ?>

    <?php if (isset($advisory['avalanche_watch'])): ?>
      <!-- Avalanche Watch row -->
      <div id="watch-row" class="advisory-row">
        <img src="<?php print '/' . path_to_theme() . '/img/row/heading-awatch.jpg'; ?>" class="advisory-row-heading" />
        <h3 class="advisory-heading">avalanche watch</h3>
        <?php print $advisory['avalanche_watch']; ?>
      </div>
    <?php endif; ?>

    <?php if (isset($advisory['avalanche_sab'])): ?>
      <!-- Special Avalanche Bulletin row -->
      <div id="sab-row" class="advisory-row">
        <img src="<?php print '/' . path_to_theme() . '/img/row/heading-asab.jpg'; ?>" class="advisory-row-heading">
        <h3 class="advisory-heading">special avalanche bulletin</h3>
        <?php print $advisory['avalanche_sab']; ?>
      </div>
    <?php endif; ?>

    <div id="sponsor-row" class="advisory-row">
      <center><?php print views_embed_view('spad','block_adv_ads_slc', $advisory['region_id']); ?></center>
    </div>

    <?php if (isset($advisory['special_announcement'])): ?>
      <!-- Special Announcement row -->
      <div id="announcement-row" class="advisory-row">
          <img src="<?php print '/' . path_to_theme() . '/img/row/heading-sa.jpg'; ?>" class="advisory-row-heading" />
          <h3 class="advisory-heading">special announcement</h3>
          <?php print $advisory['special_announcement']; ?>
      </div>
    <?php endif; ?>

    <?php if (isset($advisory['current_conditions'])): ?>
      <!-- Current conditions row -->
      <div id="current-conditions-row" class="advisory-row">
        <img src="<?php print '/' . path_to_theme() . '/img/row/heading-cc.jpg' ?>" class="advisory-row-heading" />
        <h3 class="advisory-heading">current conditions</h3>
        <?php print $advisory['current_conditions']; ?>
      </div>
    <?php endif; ?>

    <?php if (isset($advisory['recent_activity'])): ?>
      <!-- Recent Activity row -->
      <div id="recent-activity-row" class="advisory-row">
        <img src="<?php print '/' . path_to_theme() . '/img/row/heading-ra.jpg' ?>" class="advisory-row-heading" />
        <h3 class="advisory-heading">recent activity</h3>
        <?php print $advisory['recent_activity']; ?>
      </div>
    <?php endif; ?>

    <?php require_once(path_to_theme() . '/inc/advisory.inc'); ?>
    <?php if (isset($advisory['problem_1']['type'])) { print _build_problem_html($advisory['problem_1'], 'Avalanche Problem 1', $advisory['elevation_labels']); } ?>
    <?php if (isset($advisory['problem_2']['type'])) { print _build_problem_html($advisory['problem_2'], 'Avalanche Problem 2', $advisory['elevation_labels']); } ?>
    <?php if (isset($advisory['problem_3']['type'])) { print _build_problem_html($advisory['problem_3'], 'Avalanche Problem 3', $advisory['elevation_labels']); } ?>

    <?php if (isset($advisory['mountain_weather'])): ?>
      <!-- Weather row -->
      <div id="weather-row" class="advisory-row">
        <img src="<?php print '/' . path_to_theme() . '/img/row/heading-mw.jpg' ?>" class="advisory-row-heading" />
        <h3 class="advisory-heading">weather</h3>
        <?php print $advisory['mountain_weather']; ?>
      </div>
    <?php endif; ?>

    <?php if (isset($advisory['general_announcements'])): ?>
      <!-- General Announcements row -->
      <div id="general-announcements-row" class="advisory-row">
        <h3 class="advisory-heading">general announcements</h3>
        <?php print $advisory['general_announcements']; ?>
      </div>
    <?php endif; ?>
  </div>

  <?php if ($links = render($content['links'])): ?>
    <nav<?php print $links_attributes; ?>><?php print $links; ?></nav>
  <?php endif; ?>

  <?php print render($content['comments']); ?>

  <?php print render($title_suffix); ?>
</article>
<!-- End: generated from .../themes/uac_responsive/templates/node--advisory.tpl.php -->