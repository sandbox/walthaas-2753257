<?php
/**
 * @file
 * UAC Responsive implementation to display a node containing
 * an avalanche warning.
 */

require_once(DRUPAL_ROOT . '/' . path_to_theme() . '/inc/node-common.inc');

?><!-- Begin: Generated from .../themes/uac_responsive/templates/node--warning.tpl.php -->

<?php
$stamp=$node->field_warning_times['und'][0]['value'];
$start=new DateTime($stamp, new DateTimeZone('UTC'));
$start->setTimezone(new DateTimeZone('MST'));
$starttime=strtoupper($start->format('H:i A T D M j Y'));

$stamp2=$node->field_warning_times['und'][0]['value2'];
$end=new DateTime($stamp2, new DateTimeZone('UTC'));
$end->setTimezone(new DateTimeZone('MST'));
$endtime=strtoupper($end->format('H:i A T D M j Y'));
?>
<p>AVALANCHE <?php print $node->field_w_type['und'][0]['value'];?><br>
Start Time: <?php print $starttime;?><br>
End Time: <?php print $endtime;?><br>
<?php
$zones=$node->field_nws_zones['und'];
?>
For Zones: <?php foreach($zones as $zone){echo($zone[value].", ");}?><br>
content below</p>
<hr>
<p>BULLETIN – IMMEDIATE BROADCAST REQUESTED<br>
BACKCOUNTRY AVALANCHE WARNING<br>
FOREST SERVICE UTAH AVALANCHE CENTER SALT LAKE CITY UT<br>
RELAYED BY NATIONAL WEATHER SERVICE SALT LAKE CITY UT<br>
<?php print $starttime;?></p>

<p>THE FOLLOWING MESSAGE IS TRANSMITTED AT THE REQUEST OF THE FOREST SEVICE UTAH AVALANCHE CENTER.</p>

<p>THE FOREST SERVICE UTAH AVALANCHE CENTER IN SALT LAKE CITY HAS <?php print $node->field_status['und'][0]['value'];?> A BACKCOUNTRY AVALANCHE <?php print $node->field_w_type['und'][0]['value'];?>.</p>

<p>* TIMING…<?php print $node->field_timing['und'][0]['value'];?></p>
<p>* AFFECTED AREA…<?php print $node->field_affected_area['und'][0]['value'];?></p>
<p>* AVALANCHE DANGER…<?php print $node->field_warning_danger['und'][0]['value'];?></p>
<p>* IMPACTS…<?php print $node->field_impacts['und'][0]['value'];?></p>

<p>BACKCOUNTRY TRAVELERS SHOULD CONSULT WWW.UTAHAVALANCHECENTER.ORG OR CALL 1-888-999-4019 FOR MORE DETAILED INFORMATION.</p>

<p>THIS WARNING DOES NOT APPLY TO SKI AREAS WHERE AVALANCHE HAZARD REDUCTION MEASURES ARE PERFORMED.</p>

<p>$$</p>
</div>

  <?php if ($links = render($content['links'])): ?>
    <nav id="link-wrapper"<?php print $links_attributes; ?>><?php print $links; ?></nav>
  <?php endif; ?>


  <?php print render($title_suffix); ?>
</article>
<!-- End: Generated from .../themes/uac_responsive/templates/node--warning.tpl.php -->