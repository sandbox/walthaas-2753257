<?php
/**
 * @file
 * UAC Responsive implementation to display a single Drupal page.
 * Adapted from the at_core page.tpl.php
 *
 * Available variables:
 *
 * Adaptivetheme supplied variables:
 * - $site_logo: Themed logo - linked to front with alt attribute.
 * - $site_name: Site name linked to the homepage.
 * - $site_name_unlinked: Site name without any link.
 * - $hide_site_name: Toggles the visibility of the site name.
 * - $visibility: Holds the class .element-invisible or is empty.
 * - $primary_navigation: Themed Main menu.
 * - $secondary_navigation: Themed Secondary/user menu.
 * - $primary_local_tasks: Split local tasks - primary.
 * - $secondary_local_tasks: Split local tasks - secondary.
 * - $tag: Prints the wrapper element for the main content.
 * - $is_mobile: Mixed, requires the Mobile Detect or Browscap module to return
 *   TRUE for mobile.  Note that tablets are also considered mobile devices.
 *   Returns NULL if the feature could not be detected.
 * - $is_tablet: Mixed, requires the Mobile Detect to return TRUE for tablets.
 *   Returns NULL if the feature could not be detected.
 * - *_attributes: attributes for various site elements, usually holds id, class
 *   or role attributes.
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Core Regions:
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['header']: Items for the header region.
 * - $page['footer']: Items for the footer region.
 *
 * UAC Responsive Regions:
 * - $page['header_middle']:
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see template_process()
 * @see adaptivetheme_preprocess_page()
 * @see adaptivetheme_process_page()
 */
?><!-- Generated from .../themes/uac_responsive/templates/page.tpl.php -->
<?php drupal_add_js('/' . path_to_theme() . '/js/uac_responsive.js', 'file'); ?>
<div id="page-wrapper">
  <div id="page" class="container <?php print $classes; ?>">

    <!-- !Leaderboard Region -->
    <?php print render($page['leaderboard']); ?>

    <header<?php print $header_attributes; ?>>

      <?php if ($site_logo || $site_name || $site_slogan): ?>
        <!-- !Branding -->
        <div<?php print $branding_attributes; ?>>

          <?php if ($site_logo): ?>
            <div id="logo">
              <?php print $site_logo; ?>
            </div>
          <?php endif; ?>

          <?php if ($site_name || $site_slogan): ?>
            <!-- !Site name and Slogan -->
            <div<?php print $hgroup_attributes; ?>>

              <?php if ($site_name): ?>
                <h1<?php print $site_name_attributes; ?>><?php print $site_name; ?></h1>
              <?php endif; ?>

              <?php if ($site_slogan): ?>
                <h2<?php print $site_slogan_attributes; ?>><?php print $site_slogan; ?></h2>
              <?php endif; ?>

            </div>
          <?php endif; ?>

        </div>
      <?php endif; ?>

      <!-- !Header Region -->
      <?php print render($page['sponsor_ads']); ?>
      <?php $priority_one = render($page['priority_one']); print $priority_one; ?>
      <?php print render($page['header_middle']); ?>

      <!-- !Navigation -->
      <!-- menu-bar is empty on desktop -->
      <?php print render($page['menu_bar']); ?>
      <?php if ($primary_navigation): ?>
        <nav id="main-menu">
        <?php print theme('nice_menus_main_menu'); ?>
        </nav> <!-- #main-menu -->
      <?php endif; ?>
    </header>

    <?php if ($secondary_navigation): print $secondary_navigation; endif; ?>

    <!-- !Messages and Help -->
    <?php print $messages; ?>
    <?php print render($page['help']); ?>

    <!-- !Secondary Content Region -->
    <?php print render($page['secondary_content']); ?>

    <div id="columns" class="columns clearfix">
      <main id="content-column" class="content-column" role="main">
        <div class="content-inner">

          <!-- !Highlighted region -->
          <?php print render($page['highlighted']); ?>

          <<?php print $tag; ?> id="main-content">

          <?php print render($title_prefix); // Does nothing by default in D7 core ?>

          <!-- !Main Content -->
          <?php
            if ($is_front) {
              require_once(DRUPAL_ROOT . '/' . path_to_theme() . '/inc/front.inc');
            }
            else {
              require_once(DRUPAL_ROOT . '/' . path_to_theme() . '/inc/not_front.inc');
            }
          ?>
          </<?php print $tag; ?>><!-- /end #main-content -->

          <!-- !Content Aside Region-->
          <?php print render($page['content_aside']); ?>

        </div><!-- /end .content-inner -->
      </main><!-- /end #content-column -->

      <!-- !Sidebar Regions -->
      <?php $sidebar_first = render($page['sidebar_first']); print $sidebar_first; ?>
      <?php $sidebar_second = render($page['sidebar_second']); print $sidebar_second; ?>

    </div><!-- /end #columns -->

    <!-- !Tertiary Content Region -->
    <?php print render($page['tertiary_content']); ?>

  <!-- !Footer -->
  <?php if ($page['footer'] || $attribution): ?>
    <div id="footer-wrapper">
      <nav id="footer-links">
        <a href="http://instagram.com/utavy"><img src="<?php print '/' . path_to_theme() . '/img/ig25x25.png' ?>" /></a>
        <a href="https://www.facebook.com/Utah.Avalanche.Center"><img src="<?php print '/' . path_to_theme() . '/img/logo_facebook_25x25.png' ?>" /></a>
        <a href="https://twitter.com/UACwasatch"><img src="<?php print '/' . path_to_theme() . '/img/logo_twitter_25x25.png' ?>" /></a>
        <?php if ($variables['search_box']) print $variables['search_box']; ?>
      </nav>
      <footer<?php print $footer_attributes; ?>>
        <?php print render($page['footer']); ?>
        <?php print $attribution; ?>
      </footer>
    </div>
  <?php endif; ?>

  </div>
</div>

<!-- Copied from uac theme, but function unknown -->
<div style="display:none;" id="dialog-confirm" title="<?php print $dialog_title; ?>"> <?php print $dialog_content; ?> </div>
<div style="display:none;" id="dialog-hash"><?php print $dialog_hash; ?></div>

<!-- Copied from uac theme -->
<?php
  if (arg(0) == 'cart' && arg(1) == 'checkout' && arg(2) == 'complete') {
    global $user;
    if (user_is_anonymous()) {
      // Delete the session cookie set by Ubercart. This is problematic because the user account is deleted,
      // but they still have a session. Drupal gets confused if they try to purchase something again.
      foreach ($_COOKIE as $k => $v) {
        if (in_array(substr($k, 0, 4), array('SESS', 'SSES'))) {
          _drupal_session_destroy($v);
        }
      }
    }
  }
