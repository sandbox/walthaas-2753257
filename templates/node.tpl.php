<?php
/**
 * @file
 * UAC Responsive implementation to display a node.
 */

require_once(DRUPAL_ROOT . '/' . path_to_theme() . '/inc/node-common.inc');

?><!-- Begin: generated from .../themes/uac_responsive/templates/node.tpl.php -->
    <!-- render($content) -->
    <?php print render($content); ?>
  </div>
  <!-- render($content['links']) -->
  <?php if ($links = render($content['links'])): ?>
    <nav id="link-wrapper"<?php print $links_attributes; ?>><?php print $links; ?></nav>
  <?php endif; ?>
  <!-- render($content['comments']) -->
  <?php print render($content['comments']); ?>

    <!-- render($title_suffix) -->
  <?php print render($title_suffix); ?>
</article>
<!-- End: generated from .../themes/uac_responsive/templates/node.tpl.php -->