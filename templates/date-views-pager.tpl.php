<?php
/**
 * @file
 * UAC Responsive template file for the date views pager.
 *
 * Variables available:
 *
 * $plugin: The pager plugin object. This contains the view.
 *
 * $plugin->view
 *   The view object for this navigation.
 *
 * $nav_title
 *   The formatted title for this view. In the case of block
 *   views, it will be a link to the full view, otherwise it will
 *   be the formatted name of the year, month, day, or week.
 *
 * $prev_url
 * $next_url
 *   Urls for the previous and next calendar pages. The links are
 *   composed in the template to make it easier to change the text,
 *   add images, etc.
 *
 * $prev_options
 * $next_options
 *   Query strings and other options for the links that need to
 *   be used in the l() function, including rel=nofollow.
 */
?>
<!-- Begin: Generated from .../themes/uac_responsive/date-views-pager.tpl.php -->
<?php if (!empty($pager_prefix)) print $pager_prefix; ?>
<div class="date-nav-wrapper clearfix<?php if (!empty($extra_classes)) print $extra_classes; ?>">
  <div class="date-nav item-list">
    <div class="date-heading">
      <?php
        $date_info = $plugin->view->date_info;
        $format = !empty($format) ? $format : (empty($date_info->mini) ? 'F Y' : 'F');
        $title = date_format_date($date_info->min_date, 'custom', $format);
      ?>
      <?php if (!empty($prev_url)) : ?>
        <?php print l('<img src="/' . path_to_theme() . '/img/arrow-left.png" alt="Previous" />', $prev_url, array('html' => TRUE, 'attributes' => array('class' => 'uac_responsive-date-prev'))); ?>
      <?php endif; ?>
      <span class="date-title"><?php print $title ?></span>
    <?php if (!empty($next_url)) : ?>
        <?php print l('<img src="/' . path_to_theme() . '/img/arrow-right.png" alt="Next">', $next_url, array('html' => TRUE, 'attributes' => array('class' => 'uac_responsive-date-next'))); ?>
    <?php endif; ?>
    </div>
  </div>
</div>
<!-- End: Generated from .../themes/uac_responsive/date-views-pager.tpl.php -->
