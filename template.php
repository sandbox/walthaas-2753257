<?php

/**
 * @file
 * Process theme data.
 *
 * Use this file to run your theme specific implimentations of theme functions,
 * such preprocess, process, alters, and theme function overrides.
 *
 * Preprocess and process functions are used to modify or create variables for
 * templates and theme functions. They are a common theming tool in Drupal, often
 * used as an alternative to directly editing or adding code to templates. Its
 * worth spending some time to learn more about these functions - they are a
 * powerful way to easily modify the output of any template variable.
 *
 * Preprocess and Process Functions SEE: http://drupal.org/node/254940#variables-processor
 */

/**
 * Add body classes if certain regions have content.
 */
function uac_responsive_preprocess_html(&$variables) {

  if ($is_front || arg(0) == 'advisory') {
    $meta_app_store_banner = array(
        '#type' => 'html_tag',
        '#tag' => 'meta',
        '#attributes' => array(
            'content' => 'app-id=605579982',
            'name' => 'apple-itunes-app'
        ),
    );
    drupal_add_html_head($meta_app_store_banner, 'meta_app_store_banner');
  }
}

function uac_responsive_preprocess_page(&$variables) {
  // If the user doesn't have permission to use search, don't show the search box.
  if (!user_access('search content')) {
    unset($variables['search_box']);
  }

  // 16739 is the node we're using for popup/dialog data.
  $n = node_load(16739);
  if ($n->status == 1 && user_is_anonymous()) {
    drupal_add_library('system', 'ui.dialog');
    drupal_add_js('/' . path_to_theme() . '/js/dialog.load.js', 'file');
    $variables['dialog_title'] = $n->title;
    $variables['dialog_content'] = $n->body['und'][0]['value'];
    $variables['dialog_hash'] = $n->changed;
  }

  if (arg(0) == 'cart' && arg(1) == 'checkout' && arg(2) == 'review') {
    drupal_add_js('/sites/all/themes/uac/js/uc.js', 'file');
  }
}

/**
 * Override or insert variables into the page template.
 */
function uac_responsive_process_page(&$variables) {

  // Remove the 'clearfix' class from $variables['branding_attributes']
  if ($variables['branding_attributes']) {
    $p = strpos($variables['branding_attributes'], 'clearfix');
    if ($p) {
      $r = substr_replace($variables['branding_attributes'], '', $p , 8);
      $variables['branding_attributes'] =  $r;
    }
  }

  // Always print the site name and slogan, but if they are toggled off, we'll
  // just hide them visually.
  $variables['hide_site_name']   = theme_get_setting('toggle_name') ? FALSE : TRUE;
  $variables['hide_site_slogan'] = theme_get_setting('toggle_slogan') ? FALSE : TRUE;
  if ($variables['hide_site_name']) {
    // If toggle_name is FALSE, the site_name will be empty, so we rebuild it.
    $variables['site_name'] = filter_xss_admin(variable_get('site_name', 'Drupal'));
  }
  if ($variables['hide_site_slogan']) {
    // If toggle_site_slogan is FALSE, the site_slogan will be empty, so we rebuild it.
    $variables['site_slogan'] = filter_xss_admin(variable_get('site_slogan', ''));
  }
  // Since the title and the shortcut link are both block level elements,
  // positioning them next to each other is much simpler with a wrapper div.
  if (!empty($variables['title_suffix']['add_or_remove_shortcut']) && $variables['title']) {
    // Add a wrapper div using the title_prefix and title_suffix render elements.
    $variables['title_prefix']['shortcut_wrapper'] = array(
      '#markup' => '<div class="shortcut-wrapper clearfix">',
      '#weight' => 100,
    );
    $variables['title_suffix']['shortcut_wrapper'] = array(
      '#markup' => '</div>',
      '#weight' => -99,
    );
    // Make sure the shortcut link is the first item in title_suffix.
    $variables['title_suffix']['add_or_remove_shortcut']['#weight'] = -100;
  }

  if (isset($variables['node'])) {
    $node = $variables['node'];
    if ($node->type == 'advisory') {
      $variables['theme_hook_suggestions'][] = 'page__advisory';
    } else if ($node->type == 'uac_class') {
      $variables['theme_hook_suggestions'][] = 'page__uac_class';
    }
  }
  //dpm($variables);
}


/**
 * Implements hook_preprocess_maintenance_page().
 */
function uac_responsive_preprocess_maintenance_page(&$variables) {
  // By default, site_name is set to Drupal if no db connection is available
  // or during site installation. Setting site_name to an empty string makes
  // the site and update pages look cleaner.
  // @see template_preprocess_maintenance_page
  if (!$variables['db_is_active']) {
    $variables['site_name'] = '';
  }
  drupal_add_css(drupal_get_path('theme', 'bartik') . '/css/maintenance-page.css');
}


/**
 * Override or insert variables into the maintenance page template.
 */
function uac_responsive_process_maintenance_page(&$variables) {
  // Always print the site name and slogan, but if they are toggled off, we'll
  // just hide them visually.
  $variables['hide_site_name']   = theme_get_setting('toggle_name') ? FALSE : TRUE;
  $variables['hide_site_slogan'] = theme_get_setting('toggle_slogan') ? FALSE : TRUE;
  if ($variables['hide_site_name']) {
    // If toggle_name is FALSE, the site_name will be empty, so we rebuild it.
    $variables['site_name'] = filter_xss_admin(variable_get('site_name', 'Drupal'));
  }
  if ($variables['hide_site_slogan']) {
    // If toggle_site_slogan is FALSE, the site_slogan will be empty, so we rebuild it.
    $variables['site_slogan'] = filter_xss_admin(variable_get('site_slogan', ''));
  }
}


/**
 * Override or insert variables into the node template.
 */
function uac_responsive_preprocess_node(&$variables) {
  $node = $variables['node'];
  if ($variables['view_mode'] == 'full') {
    $variables['classes_array'][] = 'node-full';
    if ($variables['node']->type == 'advisory') {
      require_once('inc/advisory.inc');
      $variables['advisory'] = _get_advisory_data($variables['node']);
      $variables['overall_rose'] = $node->field_overall_rose['und'][0]['img_detailed_rose'];
    }
  }
}

/**
 * Override or insert variables into the block template.
 */
function uac_responsive_preprocess_block(&$variables) {
  if ($variables['block_html_id'] == 'block-views-spad-block-1') {
    //dpr($variables['elements'], FALSE, 'elements');
  }

  // In the header region visually hide block titles.
  if ($variables['block']->region == 'header') {
    $variables['title_attributes_array']['class'][] = 'element-invisible';
  }
}
function uac_responsive_process_block(&$variables) {
  if ($variables['block_html_id'] == 'block-views-spad-block-1') {
    //dpr($variables['elements'], FALSE, 'elements');
  }

}

/**
 * Implements theme_field__field_type().
 */
function uac_responsive_field__taxonomy_term_reference($variables) {
  $output = '';

  // Render the label, if it's not hidden.
  if (!$variables['label_hidden']) {
    $output .= '<h3 class="field-label">' . $variables['label'] . ': </h3>';
  }

  // Render the items.
  $output .= ($variables['element']['#label_display'] == 'inline') ? '<ul class="links inline">' : '<ul class="links">';
  foreach ($variables['items'] as $delta => $item) {
    $output .= '<li class="taxonomy-term-reference-' . $delta . '"' . $variables['item_attributes'][$delta] . '>' . drupal_render($item) . '</li>';
  }
  $output .= '</ul>';

  // Render the top-level DIV.
  $output = '<div class="' . $variables['classes'] . (!in_array('clearfix', $variables['classes_array']) ? ' clearfix' : '') . '"' . $variables['attributes'] .'>' . $output . '</div>';

  return $output;
}


/**
 * Override default form element placement.
 *
 * 1. Move description directly below label and before input element.
 *
 */
function uac_responsive_form_element($variables) {
  $element = &$variables['element'];
  // This is also used in the installer, pre-database setup.
  $t = get_t();

  // This function is invoked as theme wrapper, but the rendered form element
  // may not necessarily have been processed by form_builder().
  $element += array(
    '#title_display' => 'before',
  );

  // Add element #id for #type 'item'.
  if (isset($element['#markup']) && !empty($element['#id'])) {
    $attributes['id'] = $element['#id'];
  }
  // Add element's #type and #name as class to aid with JS/CSS selectors.
  $attributes['class'] = array('form-item');
  if (!empty($element['#type'])) {
    $attributes['class'][] = 'form-type-' . strtr($element['#type'], '_', '-');
  }
  if (!empty($element['#name'])) {
    $attributes['class'][] = 'form-item-' . strtr($element['#name'], array(' ' => '-', '_' => '-', '[' => '-', ']' => ''));
  }
  // Add a class for disabled elements to facilitate cross-browser styling.
  if (!empty($element['#attributes']['disabled'])) {
    $attributes['class'][] = 'form-disabled';
  }
  $output = '<div' . drupal_attributes($attributes) . '>' . "\n";

  // If #title is not set, we don't display any label or required marker.
  if (!isset($element['#title'])) {
    $element['#title_display'] = 'none';
  }
  $prefix = isset($element['#field_prefix']) ? '<span class="field-prefix">' . $element['#field_prefix'] . '</span> ' : '';
  $suffix = isset($element['#field_suffix']) ? ' <span class="field-suffix">' . $element['#field_suffix'] . '</span>' : '';

  switch ($element['#title_display']) {
    case 'before':
    case 'invisible':
      $output .= ' ' . theme('form_element_label', $variables);
      if (!empty($element['#description'])) {
        $output .= '<div class="description">' . $element['#description'] . "</div>\n";
      }
      $output .= ' ' . $prefix . $element['#children'] . $suffix . "\n";
      break;

    case 'after':
      if (!empty($element['#description'])) {
        $output .= '<div class="description">' . $element['#description'] . "</div>\n";
      }
      $output .= ' ' . $prefix . $element['#children'] . $suffix;
      $output .= ' ' . theme('form_element_label', $variables) . "\n";
      break;

    case 'none':
    case 'attribute':
      // Output no label and no required marker, only the children.
      $output .= ' ' . $prefix . $element['#children'] . $suffix . "\n";
      break;
  }

  $output .= "</div>\n";

  return $output;
}

function uac_responsive_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id == 'search_form'){
    unset($form['basic']['keys']['#title']);
    $form['basic']['submit']['#value'] = '';
  }
}


/**
 * Formats a product's price.
 *
 * @param $variables
 *   An associative array containing:
 *   - element: An associative array render element containing:
 *     - #value: Price to be formatted.
 *     - #attributes: (optional) Array of attributes to apply to enclosing DIV.
 *     - #title: (optional) Title to be used as label.
 *
 * @ingroup themeable
 */
function uac_responsive_uc_product_price($variables) {
  $element = $variables['element'];
  $price = $element['#value'];
  $attributes = isset($element['#attributes']) ? $element['#attributes'] : array();
  $label = isset($element['#title']) ? $element['#title'] : '';

  if (isset($attributes['class'])) {
    array_unshift($attributes['class'], 'product-info');
  }
  else {
    $attributes['class'] = array('product-info');
  }

  $output = '<div ' . drupal_attributes($attributes) . '>';
  if (intval($price) == 0) {
    $label = '';
    $price = 'FREE';
  }
  if ($label) {
    $output .= '<span class="uc-price-label">' . $label . '</span> ';
  }
  $vars = array('price' => $price);
  if (!empty($element['#suffixes'])) {
    $vars['suffixes'] = $element['#suffixes'];
  }
  $output .= theme('uc_price', $vars);
  $output .= drupal_render_children($element);
  $output .= '</div>';

  return $output;
}

/**
 * Displays a price in the standard format and with consistent markup.
 *
 * @param $variables
 *   An associative array containing:
 *   - price: A numerical price value.
 *   - suffixes: An array of suffixes to be attached to this price.
 *
 * @ingroup themeable
 */
function uac_responsive_uc_price($variables) {
  if ($variables['price'] != 'FREE') {
    $output = '<span class="uc-price">' . uc_currency_format($variables['price']) . '</span>';
  } else {
    $output = '<span class="uc-price">FREE</span>';
  }
  if (!empty($variables['suffixes'])) {
    $output .= '<span class="price-suffixes">' . implode(' ', $variables['suffixes']) . '</span>';
  }
  return $output;
}
